//
//  AppDelegate.h
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

