//
//  AppDelegate.m
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import "AppSDKManager.h"

@interface AppDelegate () <AppSDKManagerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[AppSDKManager sharedInstance] registerAppForPushNotification];
    [[AppSDKManager sharedInstance] setDelegate:self];
    
    return YES;
}


- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[AppSDKManager sharedInstance] didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    [AppSDKManager printDeviceTokenFromData:deviceToken];
}


#pragma mark - UNUserNotificationCenter Delegate Methods

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {

    [[AppSDKManager sharedInstance] didReceiveNotificationResponse:response];
    completionHandler();
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {

    [[AppSDKManager sharedInstance] willPresentForegroundNotification:notification];
    completionHandler(UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge);
}


@end
