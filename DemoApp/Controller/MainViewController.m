//
//  MainViewController.m
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import "MainViewController.h"
#import "DataManager.h"
#import "AppSDKManager.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma UI Events

- (IBAction)productViewedButtonPressedEvent:(UIButton *)sender {
    
    [[AppSDKManager sharedInstance] trackEvent:@"Product Viewed" withPayload:[DataManager getProductDetails]];
    [[AppSDKManager sharedInstance] profilePushPayload:[DataManager getUserProfile]];
    
    NSLog(@"Event Captured.");
}



@end
