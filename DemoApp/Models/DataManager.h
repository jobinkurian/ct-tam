//
//  DataManager.h
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataManager : NSObject

+ (NSDictionary *)getProductDetails;

+ (NSDictionary *)getUserProfile;

@end

NS_ASSUME_NONNULL_END
