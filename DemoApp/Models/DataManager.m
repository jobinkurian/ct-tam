//
//  DataManager.m
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import "DataManager.h"

@implementation DataManager

+ (NSDictionary *)getProductDetails {

    NSDictionary *productDetails = @{
        @"Product Id" : @(1),
        @"Procuct Image" : @"https://d35fo82fjcw0y8.cloudfront.net/2018/07/26020307/customer-success-clevertap.jpg",
        @"Procuct Name" : @"CleverTap",
    };
    return productDetails;
}

+ (NSDictionary *)getUserProfile {
    NSDictionary *userProfile = @{
        @"Email": @"ti.jobinkurian@gmail.com",
    };
    return userProfile;
}

@end
