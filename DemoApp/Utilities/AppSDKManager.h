//
//  AppSDKManager.h
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import <Foundation/Foundation.h>
#import <CleverTapSDK/CleverTap.h>


NS_ASSUME_NONNULL_BEGIN

@protocol AppSDKManagerDelegate <NSObject>
@optional
/**
 @brief Delegate method is to handle deeplink with the url parameters
 */
- (void)handleDeeplinkActionWithURLString:(NSString *)deeplinkURLString
                         andCustomPayload:(NSDictionary *_Nullable)customPayload;
@end

@interface AppSDKManager : NSObject


@property (nonatomic, weak) id <AppSDKManagerDelegate> delegate;

+ (instancetype)sharedInstance;

/**
 @brief To initialise the SDK.
*/
- (void)initializeSDK;

/**
 @brief To register the app for push notification.
*/
- (void)registerAppForPushNotification;

/**
 @brief This method is called when device token is generated.
*/
- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

/**
 @brief This method will be called on the delegate only if the application is in the foreground.
*/
- (void)willPresentForegroundNotification:(UNNotification *)notification;

/**
 @brief This method will be called when the user clicks on the push notification.
*/
- (void)didReceiveNotificationResponse:(UNNotificationResponse *)response;

/**
 @brief This method is used to track event.
 @param userProfile The profile info of the user.
 */
- (void)profilePushPayload:(NSDictionary *)userProfile;

/**
 @brief This method is used to track event.
 @param eventName The event name to be tracked.
 @param payload The event's meta info to be passed along with the event.
 */
- (void)trackEvent:(NSString *)eventName withPayload:(NSDictionary * __nullable)payload;


#pragma Helper Methods

+ (void)printDeviceTokenFromData:(NSData *)deviceToken;

@end

NS_ASSUME_NONNULL_END
