//
//  AppSDKManager.m
//  DemoApp
//
//  Created by Jobin Kurian on 02/03/21.
//

#import "AppSDKManager.h"
#import <UserNotifications/UserNotifications.h>

@implementation AppSDKManager

+ (instancetype)sharedInstance {
    static AppSDKManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - App Delegate Changes

- (void)initializeSDK {
    
}

- (void)registerAppForPushNotification {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if( !error ){
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
        }
    }];
}

- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[CleverTap sharedInstance] setPushToken:deviceToken];
}

- (void)willPresentForegroundNotification:(UNNotification *)notification {
    [[CleverTap sharedInstance] handleNotificationWithData:notification.request.content.userInfo];
}

- (void)didReceiveNotificationResponse:(UNNotificationResponse *)response {
    [[CleverTap sharedInstance] handleNotificationWithData:response.notification.request.content.userInfo openDeepLinksInForeground: YES];
}

- (void)profilePushPayload:(NSDictionary *)userProfile {
    [[CleverTap sharedInstance] profilePush:userProfile];
}

- (void)trackEvent:(NSString *)eventName withPayload:(NSDictionary * __nullable)payload {
    
    if (payload != nil) {
        [[CleverTap sharedInstance] recordEvent:eventName withProps:payload];
    }
    else {
        [[CleverTap sharedInstance] recordEvent:eventName];
    }
}


#pragma Helper Methods

+ (void)printDeviceTokenFromData:(NSData *)deviceToken {
    NSUInteger dataLength = deviceToken.length;
    if (dataLength == 0) {
        return;
    }
    const unsigned char *dataBuffer = (const unsigned char *)deviceToken.bytes;
    NSMutableString *hexString = [NSMutableString stringWithCapacity:(dataLength * 2)];
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendFormat:@"%02x", dataBuffer[i]];
    }
    NSString *deviceTokenString = [hexString copy];
    NSLog(@"[DemoApp] DeviceToken = %@", deviceTokenString);
}

@end
