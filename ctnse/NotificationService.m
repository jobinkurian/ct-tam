//
//  NotificationService.m
//  ctnse
//
//  Created by Jobin Kurian on 02/03/21.
//

#import "NotificationService.h"

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.mediaUrlKey = @"mediaUrl";
    self.mediaTypeKey = @"mediaType";
    
    [super didReceiveNotificationRequest:request withContentHandler:contentHandler];
}


@end
